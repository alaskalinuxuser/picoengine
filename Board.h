#pragma once
#include <cmath>
#include <string>
using namespace std;

class Board {
	
private:
	char m_theBoard[64] = {'R','N','B','Q','K','B','N','R','P','P','P','P','P',
	'P','P','P','-','-','-','-','-','-','-','-','-','-','-','-','-','-',
	'-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-',
	'-','p','p','p','p','p','p','p','p','r','n','b','q','k','b','n','r'};
	
	bool m_whitesTurn = true;
	bool m_Kcastle = true;
	bool m_Qcastle = true;
	bool m_kcastle = true;
	bool m_qcastle = true;
	bool m_enPassant = false;
	char m_enPasPawn = ' ';
	int m_moveSince = 0;
	int m_turnCount = 0;


	// Public prototypes go here	
public:

	// Spawn a new board
	void setup(string boardPositions);
	
	string getBoard();
	
	string moveBoard();
	
	bool getTurn();
	
	bool getIsPassent();
	
	char getEnPassent();

};


