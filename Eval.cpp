#include <sstream>
#include "Eval.h"
#include "Moves.h"
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iostream>
#include <string>
Moves moveEvaluations;

int Eval::getEval(string thatBoard) {
	int picoEval = 0;
	
	// Modified from http://chessprogramming.wikispaces.com/Simplified+evaluation+function
	// This website closed as of January 2019.
    int pawnBoard[]=
					{5, 5,  5,  5,  5,  5,  5,  5,
                    2,  2,  2,  2,  2,  2,  2,  2,
                    0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  1,  2,  3,  0,  0,  0,
                    2,  0,  1,  3,  2,  1,  0,  2,
                    1,  1,  1, -1, -1,  1,  1,  1,
                    0,  0,  0,  0,  0,  0,  0,  0};
    int rookBoard[]=
					{ 1, 1,  1,  1,  1,  1,  1,  1,
                     1,  1,  1,  1,  1,  1,  1,  1,
                    -1,  0,  0,  0,  0,  0,  0, -1,
                    -1,  0,  0,  0,  0,  0,  0, -1,
                    -1,  0,  0,  0,  0,  0,  0, -1,
                    -1,  0,  0,  0,  0,  0,  0, -1,
                    -1,  0,  0,  0,  0,  0,  0, -1,
                     1,  0,  0,  0,  0,  0,  0,  1,};
    int knightBoard[]=
					{-5, -4, -3, -3, -3, -3, -4, -5,
                     -4, -2,  0,  0,  0,  0, -2, -4,
                     -3,  0,  1,  2,  2,  1,  0, -3,
                     -3,  1,  2,  1,  1,  2,  1, -3,
                     -3,  0,  2,  1,  1,  2,  0, -3,
                     -3,  1,  1,  2,  2,  2,  1, -3,
                     -4, -2,  0,  1,  1,  0, -2, -4,
                     -5, -3, -3, -3, -3, -3, -3, -5};
    int bishopBoard[]=
					{-2, -1, -1, -1, -1, -1, -1, -2,
                     -1,  0,  0,  0,  0,  0,  0, -1,
                     -1,  0,  1,  1,  1,  1,  0, -1,
                     -2,  1,  1,  1,  1,  1,  1, -2,
                     -1,  0,  1,  1,  1,  1,  0, -1,
                     -1,  1,  1,  1,  1,  1,  1, -1,
                     -1,  1,  0,  0,  0,  0,  1, -1,
                     -2, -1, -1, -1, -1, -1, -1, -2};
    int queenBoard[]=
					{-2,-1, -1, -1, -1, -1, -1, -2,
                    -1,  0,  0,  0,  0,  0,  0, -1,
                    -1,  1,  1,  1,  1,  1,  1, -1,
                    -1,  0,  1,  1,  1,  1,  0, -1,
                    -1,  0,  1,  1,  1,  1,  0, -1,
                    -1,  1,  1,  1,  1,  1,  1, -1,
                    -1,  0,  1,  0,  0,  1,  0, -1,
                    -2, -1, -1, -1, -1, -1, -1, -2};
    int kingBoardW[]=
					{ 0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  11,  0,  10,  10,  10,  0,  0,};
    int kingBoardB[]=
					{ 0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  0,  0,  0,  0,  0, 0,
                    0,  0,  11,  10,  10,  0,  10,  0,};
                    
    
            for (int i=0;i<64;i++) {
                switch (thatBoard[i]) {
                    case 'P': picoEval+=pawnBoard[63-i];
                        break;
                    case 'R': picoEval+=rookBoard[63-i];
                        break;
                    case 'N': picoEval+=knightBoard[63-i];
                        break;
                    case 'B': picoEval+=bishopBoard[63-i];
                        break;
                    case 'Q': picoEval+=queenBoard[63-i];
                        break;
                    case 'K': picoEval+=kingBoardW[63-i];
                        break;
                    case 'p': picoEval-=pawnBoard[i];
                        break;
                    case 'r': picoEval-=rookBoard[i];
                        break;
                    case 'n': picoEval-=knightBoard[i];
                        break;
                    case 'b': picoEval-=bishopBoard[i];
                        break;
                    case 'q': picoEval-=queenBoard[i];
                        break;
                    case 'k': picoEval-=kingBoardB[i];
                        break;
                }
            }
        
	
	// Material Evaluation Deca-Pawns.
	for (int i = 0; i < 64; i++) {
                switch (thatBoard[i]) {
                    case 'N': picoEval+=30;break;
                    case 'R': picoEval+=50;break;
                    case 'B': picoEval+=30;break;
                    case 'Q': picoEval+=100;break;
                    case 'K': picoEval+=10000;break;
                    case 'P': picoEval+=10;break;
                    case 'n': picoEval-=30;break;
                    case 'r': picoEval-=50;break;
                    case 'b': picoEval-=30;break;
                    case 'q': picoEval-=100;break;
                    case 'k': picoEval-=10000;break;
                    case 'p': picoEval-=10;break;
                }} // End material evaluation.

	// Mobility evaluation
	string listMoves ="";
	int deltaChange = 0;
	try {
	listMoves = moveEvaluations.available(thatBoard, true, false, false);
		if (listMoves.size() == 0){
			deltaChange -= 10000; } else {
			deltaChange = deltaChange + listMoves.size()/15;
		}
	listMoves = moveEvaluations.available(thatBoard, false, false, false);
		if (listMoves.size() == 0){
			deltaChange += 10000; } else {
			deltaChange = deltaChange - listMoves.size()/15;
		}
	} catch (...) {
			cout << " Exception " << endl;
			} // End try/catch block
			//cout << deltaChange << " Delta " << endl;
	picoEval += deltaChange;
	return picoEval;

}
