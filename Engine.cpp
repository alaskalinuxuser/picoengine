/*  Copyright 2018 by AlaskaLinuxUser (https://thealaskalinuxuser.wordpress.com)
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*/

#include <iostream>
#include <vector>
#include <string>
#include <sstream> 
#include "Board.h"
#include "Moves.h"
using namespace std;

string engineName = "picoEngine 1.0";
string inputString = "";
bool runProgram = true;
bool styleRandom = false;
Board board;
int chosenPly = 8;
Moves movePick;
string engineMove = "";
bool calculate = true;


void inputUCI()
{
	cout << "id name " << engineName << endl;
	cout << "id author Alaskalinuxuser" << endl;
	cout << "Apache 2.0 License." << endl;
	cout << "https://thealaskalinuxuser.wordpress.com" << endl;
	// Options can go in here
	cout << "option name Style type combo default Normal var Random var Normal" << endl;
	cout << "option name Ply type combo default 8 var 1 var 2 var 3 var 4 var 5 var 6 var 7 var 8" << endl;
	// End of Options
	cout << "uciok" << endl;
}
void inputSetOption(string setString)
{
	cout << "Setting Options...." << endl;
	if (std::string::npos != setString.find("Random"))
            {
                styleRandom = true;
                cout << "Random Mode." << endl;
            }
    if (std::string::npos != setString.find("Normal"))
            {
                styleRandom = false;
                cout << "Normal Mode." << endl;
            }
    if (std::string::npos != setString.find("1"))
            {
                chosenPly = 1;
                cout << "Ply 1." << endl;
            }
    if (std::string::npos != setString.find("2"))
            {
                chosenPly = 2;
                cout << "Ply 2." << endl;
            }
    if (std::string::npos != setString.find("3"))
            {
                chosenPly = 3;
                cout << "Ply 3." << endl;
            }
    if (std::string::npos != setString.find("4"))
            {
                chosenPly = 4;
                cout << "Ply 4." << endl;
            }
    if (std::string::npos != setString.find("5"))
            {
                chosenPly = 5;
                cout << "Ply 5." << endl;
            }
    if (std::string::npos != setString.find("6"))
            {
                chosenPly = 6;
                cout << "Ply 6." << endl;
            }
    if (std::string::npos != setString.find("7"))
            {
                chosenPly = 7;
                cout << "Ply 7." << endl;
            }
    if (std::string::npos != setString.find("8"))
            {
                chosenPly = 8;
                cout << "Ply 8." << endl;
            }
	cout << "Options set." << endl;
}
void inputIsReady()
{
	// Do any initialization first.
	cout << "readyok" << endl;
}
void inputPosition(string posString)
{
  board.setup(posString);
}
void inputUCINewGame()
{
	board.setup("position fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");	
}
void inputGo()
{
	cout << "Going...." << endl;
	calculate = true;
	engineMove = "";
	engineMove = movePick.bestMove(board.moveBoard(), board.getTurn(), styleRandom, board.getIsPassent(), board.getEnPassent(), chosenPly);
	cout << "bestmove " + engineMove << endl;
}
void inputQuit()
{
	cout << "Quiting...." << endl;
	runProgram = false;
}
void inputStop()
{
	calculate = false;
	cout << "Stopped." << endl;
}
void inputPrint()
{
	string printString = "";
	printString = board.getBoard();
	cout << "info " << printString << endl;
}


int main()
{   
    cout << "Waiting...." << endl;
    
    while(runProgram) {
    getline(cin, inputString);
	
			if (inputString == "uci")
            {
                inputUCI();
            }
            else if (inputString.rfind("setoption", 0) == 0)
            {
                inputSetOption(inputString);
            }
            else if (inputString.rfind("isready", 0) == 0)
            {
                inputIsReady();
            }
            else if (inputString.rfind("ucinewgame", 0) == 0)
            {
                inputUCINewGame();
            }
            else if (inputString.rfind("position", 0) == 0)
            {
                inputPosition(inputString);
            }
            else if (inputString.rfind("go", 0) == 0)
            {
                inputGo();
            }
            else if (inputString.rfind("quit", 0) == 0)
            {
                inputQuit();
            }
            else if (inputString.rfind("print", 0) == 0)
            {
                inputPrint();
            }
            else if (inputString.rfind("stop", 0) == 0)
            {
                inputStop();
            }
		}
	     return 0;
}
