# picoEngine (pEngine)

# About

A very small open source UCI compatible chess engine written in C++. It is designed to be small, light weight, fast, and use as little ram as possible. It is not the best engine, but is playable with up to 8 ply and the option to choose Normal level of ply, or just random moves. The entire compiled engine only takes up 364.2 KB of space!

# Compiling:

If you are reading this, then you have downloaded and extracted the package already. All you must do now to compile it is:

$ ./build.sh

It comes with a precompiled version that was compiled on Ubuntu 18.04 for generic 64 bit machines.

# Features

+ UCI compatible engine!
+ Accepts both FEN and MovePos standard inputs.
+ Up to 8 ply weighted moves.
+ Has an optional random move mode for easy chess players.
+ Fast, even at 8 ply only takes a second on a modern 64 bit machine.
+ Light weight, it only takes 2 MB of ram to run this engine.
+ Small size, the (Linux 64 bit) compiled engine is only 364.2 KB in size.
+ Small source code, the entire source repository is less than 1.3 MB in size.
+ Can output moves and engine evaluation (only displayed by interfaces that support it).

# Platforms

This engine has been tested satisfactorily with:

+ Arena
+ PyChess

Should work with other chess programs that support UCI interface.

# TODO:

+ Needs opening book.
+ Could be smarter, just don't want to lose site of the lightweight, fast speed goal.

# Written by:

Originally written by the alaskalinuxuser. Read the entire developement history here:
https://thealaskalinuxuser.wordpress.com/?s=picoEngine
