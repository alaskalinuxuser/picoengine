#include <sstream>
#include "Board.h"
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>


using namespace std;
vector<string> split(string str, char delimiter) {
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;
 
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
 
  return internal;
}

void Board::setup(string boardPositions)
{
vector<string> sepVect = split(boardPositions, ' ');
  string stringBoard = "";

	if (sepVect.size() > 1) {
		// Make sure it is big enough to use.... Part of the standard is
		// to ignore anything non-useful.
			if (sepVect[1] == "fen")
            {
                // FEN board setup.
				string str2 = sepVect[2];
                for (int i = 0; (unsigned)i < str2.length(); i++) {
					char myChar = str2.at(i);
					if (isdigit(myChar))
					  { int imyChar = myChar - '0';
						for (int a = 0; a < imyChar; a++)
						 { stringBoard = stringBoard + "-"; }
					  }
					 else if (isalpha(myChar))
					  { stringBoard = stringBoard + myChar; }
					} // end for.
					cout << stringBoard << endl;
                for (int i = 0; i < 8; i++) {
					m_theBoard[i+0] = stringBoard.at(i+56);
					m_theBoard[i+8] = stringBoard.at(i+48);
					m_theBoard[i+16] = stringBoard.at(i+40);
					m_theBoard[i+24] = stringBoard.at(i+32);
					m_theBoard[i+32] = stringBoard.at(i+24);
					m_theBoard[i+40] = stringBoard.at(i+16);
					m_theBoard[i+48] = stringBoard.at(i+8);
					m_theBoard[i+56] = stringBoard.at(i+0);
					}
				if (sepVect[3].at(0) == 'w') 
				{ m_whitesTurn = true; } else { m_whitesTurn = false; }
				
				string s1 = sepVect[4];
				string K1 = "K";string Q1 = "Q";string K2 = "k";string Q2 = "q";
				if (s1.find(K1) != std::string::npos)
					{ m_Kcastle = true; } else { m_Kcastle = false; }
				if (s1.find(Q1) != std::string::npos)
					{ m_Qcastle = true; } else { m_Qcastle = false; }
				if (s1.find(K2) != std::string::npos)
					{ m_kcastle = true; } else { m_kcastle = false; }
				if (s1.find(Q2) != std::string::npos)
					{ m_qcastle = true; } else { m_qcastle = false; }
				if (sepVect[5].at(0) == '-')
					{ m_enPassant = false;} else { m_enPassant = true;}					
				m_moveSince = stoi (sepVect[6]);
				m_turnCount = stoi (sepVect[7]);
				// If also has moves after fen....
				if (sepVect.size() > 8) {
					if (sepVect[8].at(0) == 'm'){
						int l = sepVect.size();
					 for (int k = 9; k < l; k++) {
					    int first =((sepVect[k].at(0) - 'a' + 1) + (((sepVect[k].at(1) - '1') * 8) - 1));
					    int second =((sepVect[k].at(2) - 'a' + 1) + (((sepVect[k].at(3) - '1') * 8) - 1));
					    m_theBoard[second] = m_theBoard[first];
					    
					    if (m_enPassant){
							if (m_theBoard[second] == 'p' && sepVect[k].at(1) == '4'){
								if ((sepVect[k].at(1)-sepVect[k].at(3)) == 1){
									if ((sepVect[k].at(2)-sepVect[k].at(0)) == 1){
										m_theBoard[first-1] = '-';
										}}
								if ((sepVect[k].at(1)-sepVect[k].at(3)) == 1){
									if ((sepVect[k].at(0)-sepVect[k].at(2)) == 1){
										m_theBoard[first+1] = '-';
										}}	
									//cout << "EN PASSANT" << endl;
							}
							if (m_theBoard[second] == 'P' && sepVect[k].at(1) == '5'){
								if ((sepVect[k].at(3)-sepVect[k].at(1)) == 1){
									if ((sepVect[k].at(2)-sepVect[k].at(0)) == 1){
										m_theBoard[first+1] = '-';
										}}
								if ((sepVect[k].at(3)-sepVect[k].at(1)) == 1){
									if ((sepVect[k].at(0)-sepVect[k].at(2)) == 1){
										m_theBoard[first-1] = '-';
										}}	
									//cout << "EN PASSANT" << endl;
							}
							}// End checking en passant.
					    
					    m_enPassant = false; m_enPasPawn = ' ';
					    // EN PASSANT enabled
					    if (m_theBoard[second] == 'p'){
							if ((sepVect[k].at(1)-sepVect[k].at(3)) == 2){
								m_enPassant = true;
								m_enPasPawn = sepVect[k].at(2);
								 //cout << "EN PASSANT enabled " << m_enPasPawn << endl;
								}
							}
						if (m_theBoard[second] == 'P'){
							if ((sepVect[k].at(3)-sepVect[k].at(1)) == 2){
								m_enPassant = true;
								m_enPasPawn = sepVect[k].at(2);
								 //cout << "EN PASSANT enabled " << m_enPasPawn << endl;
								}
							}
						// End EN PASSANT enabled
					    
					    if (sepVect[k].size() == 5) {
							char newPiece = '-';
							if (!m_whitesTurn) {
							 newPiece = tolower(sepVect[k].at(4));
							} else {
							 newPiece = toupper(sepVect[k].at(4));
							}
						m_theBoard[second] = newPiece;
						}
						if (m_theBoard[first] == 'k' || m_theBoard[first] == 'K'){
							//cout << "King Move" << endl;
							if (first == 4 && second == 6) {
								// White Castles King Side
								m_theBoard[7] = '-';
								m_theBoard[5] = 'R';//cout << "castleWKS" << endl;
							} else if (first == 4 && second == 2) {
								// White Castles Queen Side
								m_theBoard[0] = '-';
								m_theBoard[3] = 'R';//cout << "castleWQS" << endl;
							} else if (first == 60 && second == 62) {
								// Black Castles King Side
								m_theBoard[63] = '-';
								m_theBoard[61] = 'r';//cout << "castleBKS" << endl;
							} else if (first == 60 && second == 58) {
								// Black Castles Queen Side
								m_theBoard[56] = '-';
								m_theBoard[59] = 'r';//cout << "castleBQS" << endl;
							}
						} // Not a king move.....
						
					    m_theBoard[first] = '-';
					    if (first == 4) {m_Kcastle = false; m_Qcastle = false;}
					    if (first == 60) {m_kcastle = false; m_qcastle = false;}
					    // How to handle enPassant?
					    m_whitesTurn = !m_whitesTurn;
					    m_turnCount = k/2 - 0.5;
					    }
					}// end moves setup.
				}// end FEN and MOVES.
				cout << "Position Set...." << endl;
            } // End FEN
            else if (sepVect[1] == "startpos")
            {
					 stringBoard = "RNBQKBNRPPPPPPPP--------------------------------pppppppprnbqkbnr";
					 m_whitesTurn = true;
					 m_Kcastle = true;
					 m_Qcastle = true;
					 m_kcastle = true;
					 m_qcastle = true;
					 m_enPassant = false;
					 m_enPasPawn = ' ';
					 m_moveSince = 0;
					 m_turnCount = 0;
					 int j = stringBoard.length();	
					 for (int i = 0; i < j; i++) {
					 if (i < 64){ m_theBoard[i] = stringBoard.at(i); }}
						
				if (sepVect.size() == 2){
					// All set up!
				} else if (sepVect.size() > 2) {
					if (sepVect[2] == "moves"){
						int l = sepVect.size();
					 for (int k = 3; k < l; k++) {
					    int first =((sepVect[k].at(0) - 'a' + 1) + (((sepVect[k].at(1) - '1') * 8) - 1));
					    int second =((sepVect[k].at(2) - 'a' + 1) + (((sepVect[k].at(3) - '1') * 8) - 1));
					    m_theBoard[second] = m_theBoard[first];
					    
					    if (m_enPassant){
							if (m_theBoard[second] == 'p' && sepVect[k].at(1) == '4'){
								if ((sepVect[k].at(1)-sepVect[k].at(3)) == 1){
									if ((sepVect[k].at(2)-sepVect[k].at(0)) == 1){
										m_theBoard[first-1] = '-';
										}}
								if ((sepVect[k].at(1)-sepVect[k].at(3)) == 1){
									if ((sepVect[k].at(0)-sepVect[k].at(2)) == 1){
										m_theBoard[first+1] = '-';
										}}	
									//cout << "EN PASSANT" << endl;
							}
							if (m_theBoard[second] == 'P' && sepVect[k].at(1) == '5'){
								if ((sepVect[k].at(3)-sepVect[k].at(1)) == 1){
									if ((sepVect[k].at(2)-sepVect[k].at(0)) == 1){
										m_theBoard[first+1] = '-';
										}}
								if ((sepVect[k].at(3)-sepVect[k].at(1)) == 1){
									if ((sepVect[k].at(0)-sepVect[k].at(2)) == 1){
										m_theBoard[first-1] = '-';
										}}	
									//cout << "EN PASSANT" << endl;
							}
							}// End checking en passant.
					    
					    m_enPassant = false; m_enPasPawn = ' ';
					    // EN PASSANT enabled
					    if (m_theBoard[second] == 'p'){
							if ((sepVect[k].at(1)-sepVect[k].at(3)) == 2){
								m_enPassant = true;
								m_enPasPawn = sepVect[k].at(2);
								 //cout << "EN PASSANT enabled " << m_enPasPawn << endl;
								}
							}
						if (m_theBoard[second] == 'P'){
							if ((sepVect[k].at(3)-sepVect[k].at(1)) == 2){
								m_enPassant = true;
								m_enPasPawn = sepVect[k].at(2);
								 //cout << "EN PASSANT enabled " << m_enPasPawn << endl;
								}
							}
						// End EN PASSANT enabled
					    
					    if (sepVect[k].size() == 5) {
							char newPiece = '-';
							if (!m_whitesTurn) {
							 newPiece = tolower(sepVect[k].at(4));
							} else {
							 newPiece = toupper(sepVect[k].at(4));
							}
						m_theBoard[second] = newPiece;
						}
						if (m_theBoard[first] == 'k' || m_theBoard[first] == 'K'){
							//cout << "King Move" << endl;
							if (first == 4 && second == 6) {
								// White Castles King Side
								m_theBoard[7] = '-';
								m_theBoard[5] = 'R';//cout << "castleWKS" << endl;
							} else if (first == 4 && second == 2) {
								// White Castles Queen Side
								m_theBoard[0] = '-';
								m_theBoard[3] = 'R';//cout << "castleWQS" << endl;
							} else if (first == 60 && second == 62) {
								// Black Castles King Side
								m_theBoard[63] = '-';
								m_theBoard[61] = 'r';//cout << "castleBKS" << endl;
							} else if (first == 60 && second == 58) {
								// Black Castles Queen Side
								m_theBoard[56] = '-';
								m_theBoard[59] = 'r';//cout << "castleBQS" << endl;
							}
						} // Not a king move.....
						
					    m_theBoard[first] = '-';
					    if (first == 4) {m_Kcastle = false; m_Qcastle = false;}
					    if (first == 60) {m_kcastle = false; m_qcastle = false;}
					    // How to handle enPassant?
					    m_whitesTurn = !m_whitesTurn;
					    m_turnCount = k/2 - 0.5;
					    }
						cout << "Position Set...." << endl;
					}// end moves setup.
				} // end is bigger than 2 vectors.
            } // end startpos.
    
    
    
	}// End Size of vector is greater than 1   	
}

string Board::getBoard()
{
	string printBoard = "";
	for (int i = 0; i < 64; i++){
		printBoard = printBoard + m_theBoard[i];
	}
	printBoard += " " + m_whitesTurn;
	printBoard += " " + m_Kcastle;
	printBoard += " " + m_Qcastle;
	printBoard += " " + m_kcastle;
	printBoard += " " + m_qcastle;
	printBoard += " " + m_moveSince;
	printBoard += " " + m_turnCount;
	return printBoard;
}

string Board::moveBoard() {
	string printBoard = "";
	for (int i = 0; i < 64; i++){
		printBoard = printBoard + m_theBoard[i];
	}
	return printBoard;
}

bool Board::getTurn() {

	return m_whitesTurn;

}

bool Board::getIsPassent() {

	return m_enPassant;

}

char Board::getEnPassent() {

	return m_enPasPawn;

}
