#pragma once
#include <cmath>
#include <string>
using namespace std;

class Moves {
	
private:
	char m_theBoard[64] = {'R','N','B','Q','K','B','N','R','P','P','P','P','P',
	'P','P','P','-','-','-','-','-','-','-','-','-','-','-','-','-','-',
	'-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-',
	'-','p','p','p','p','p','p','p','p','r','n','b','q','k','b','n','r'};
	
	bool m_whitesTurn = true;
	bool m_Kcastle = true;
	bool m_Qcastle = true;
	bool m_kcastle = true;
	bool m_qcastle = true;
	bool m_enPassant = false;
	string m_enPasPawn = "";
	int m_moveSince = 0;
	int m_turnCount = 0;
	bool m_makeCalc = true;


	// Public prototypes go here	
public:

	// All available moves.
	string available(string boardPositions, bool whoseTurn, bool isPass, char enPass);
	
	// Choose a move, random or weighted.
	string bestMove(string boardPositions, bool whoseTurn, bool styleRandom, bool isPass, char enPass, int plyMoves);
	
	// Weighted move, considering ply.
	string plyMove(string boardPositions, bool whoseTurn, bool isPass, char enPass, int plyMoves);
	
	// Weighted move, non-ply.
	string nonPlyMove(string boardPositions, bool whoseTurn, bool isPass, char enPass);
	
	// Is the king safe?
	bool isKingSafe(string boardPositions, bool whoseTurn);
	
	// Black night moves:
	string nightMovesB(string boardPositions, int i);
	// white night moves:
	string nightMoves(string boardPositions, int i);
	// Black rook moves:
	string rookMovesB(string boardPositions, int i);
	// white rook moves:
	string rookMoves(string boardPositions, int i);
	// Black bishop moves:
	string bishopMovesB(string boardPositions, int i);
	// white bishop moves:
	string bishopMoves(string boardPositions, int i);
	// Black queen moves:
	string queenMovesB(string boardPositions, int i);
	// white queen moves:
	string queenMoves(string boardPositions, int i);
	// Black king moves:
	string kingMovesB(string boardPositions, int i);
	// white king moves:
	string kingMoves(string boardPositions, int i);
	// Black pawn moves:
	string pawnMovesB(string boardPositions, int i, bool isPass, char enPass);
	// white pawn moves:
	string pawnMoves(string boardPositions, int i, bool isPass, char enPass);
};


