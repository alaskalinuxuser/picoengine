#!/bin/bash

# This will compile under std=c++1z (2017), std=c++14 (2014), or std=c++11 (2011) standards as well.
# If your compiler doesn't recognize the std=c++1z, you can try one of the above instead.
# If your compiler is newer, you could try std=c++17 as well.

# My compiler version:
#	alaskalinuxuser@alaskalinuxuser-OptiPlex-7010:~$ g++ --version
#	g++ (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609
#	Copyright (C) 2015 Free Software Foundation, Inc.
#	This is free software; see the source for copying conditions.  There is NO
#	warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 Board.cpp -o board.o
g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 Eval.cpp -o eval.o
g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 Moves.cpp -o moves.o
g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 Engine.cpp -o engine.o

g++ board.o eval.o moves.o engine.o -o picoEngine

echo "type ./picoEngine to launch"
exit
