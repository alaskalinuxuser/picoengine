#include <sstream>
#include "Moves.h"
#include "Eval.h"
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>


using namespace std;

Eval evaluations;

bool Moves::isKingSafe(string boardPositions, bool whoseTurn) {
        // For checking if the king is safe.
        int z;
        if (whoseTurn){
            for (int i = 0; i < 64; i++) {
                switch (boardPositions[i]) {
                    case 'K': z = i;break;
                }
            }
            int g = z%8;
            int h = z/8;
            bool notI=true;
            // Bishop or Queen
            int k;
            if (h < 7) {
                // Up diagonal moves.
                if (g < 7) {
                    k = z + 9;
                    while (boardPositions[k] == '-' && notI) {
                        if (k/8 < 7 && k%8 < 7) {
                            k = k + 9;
                        } else {notI = false;}} // While it's empty.
                    if (boardPositions[k]=='b'||boardPositions[k]=='q') {
                        return false;} // When there is an enemy.
                }
                notI = true;
                if (g > 0) {
                    k = z + 7;
                    while (boardPositions[k] == '-' && notI) {
                        if (k%8 > 0 && k/8 < 7) {
                            k = k + 7;
                        } else {notI = false;}} // While it's empty.
                    if (boardPositions[k]=='b'||boardPositions[k]=='q') {
                        return false;} // When there is an enemy.
                }}

            if (h > 0) {
                // down diagonal moves.
                notI = true;
                if (g > 0) {
                    k = z - 9;
                    while (boardPositions[k] == '-' && notI) {
                        if (k%8 > 0 && k/8 > 0) {
                            k = k - 9;
                        } else {notI = false;}} // While it's empty.
                    if (boardPositions[k]=='b'||boardPositions[k]=='q') {
                        return false;} // When there is an enemy.
                }
                notI = true;
                if (g < 7) {
                    k = z - 7;
                    while (boardPositions[k] == '-' && notI) {
                        if (k%8 < 7 && k/8 > 0) {
                            k = k - 7;
                        } else { notI = false;}} // While it's empty.
                    if (boardPositions[k]=='b'||boardPositions[k]=='q') {
                        return false;} // When there is an enemy.
                }}
            // Rook or Queen
            // Up moves
            notI = true;
            int j = 1;
            int vert = 8;
            k = z;
            if (z < 56) {
                k = z + (vert * j);
            }
            while (boardPositions[k] == '-' && notI) {
                vert += 8;
                if (k < 56) {
                    k = z + (vert * j);
                } else {notI = false;}} // While it's empty.
            if (boardPositions[k]=='r'||boardPositions[k]=='q') {
                return false;} // When there is an enemy..

            // Down moves
            notI = true;
            j = -1;
            vert = 8;
            k = z;
            if (z > 7) {
                k = z + (vert * j);
            }
            while (boardPositions[k] == '-' && notI) {
                vert += 8;
                if (k >7) {
                    k = z + (vert * j);
                } else {notI = false;}} // While it's empty.
            if (boardPositions[k]=='r'||boardPositions[k]=='q') {
                return false;} // When there is an enemy..

            // Right side....
            notI = true;
            int rj = 1;
            int rk = z;
            if (g < 7) {
                rk = z + rj;
            }
            while (boardPositions[rk] == '-' && notI) {
                rj++;
                if (rk%8 < 7) {
                    rk = z + rj;
                } else {notI = false;}} // While it's empty.
            if (boardPositions[rk]=='r'||boardPositions[rk]=='q') {
                return false;} // When there is an enemy..

            // Left side....
            notI=true;
            rj = 1;
            rk = z;
            if (g > 0) {
                rk = z - rj;
            }
            while (boardPositions[rk] == '-' && notI) {
                rj++;
                if (rk%8 > 0) {
                    rk = z - rj;
                } else {notI = false;}} // While it's empty.
            if (boardPositions[rk]=='r'||boardPositions[rk]=='q') {
                return false;} // When there is an enemy..
            // Knight
            if (h < 7 ) {
                if (g > 1 && boardPositions[z+6]=='n') {
                    return false;}
                if (g < 6 && boardPositions[z+10]=='n') {
                    return false;}}
            if (h < 6 ) {
                if (g > 0 && boardPositions[z+15]=='n') {
                    return false;}
                if (g < 7 && boardPositions[z+17]=='n') {
                    return false;}}
            if (h > 0 ) {
                if (g < 6 && boardPositions[z-6]=='n') {
                    return false;}
                if (g > 1 && boardPositions[z-10]=='n') {
                    return false;}}
            if (h > 1 ) {
                if (g < 7 && boardPositions[z-15]=='n') {
                    return false;}
                if (g > 0 && boardPositions[z-17]=='n') {
                    return false;}}
            // King check // Don't move next to another king! // Also includes pawns.
            if (h < 7 ) {
                if (boardPositions[z+8]=='k') {
                    return false;}
                if (g > 0) {
                    if (boardPositions[z+7]=='k' || boardPositions[z+7]=='p') {
                        return false;}}
                if (g < 7) {
                    if (boardPositions[z+9]=='k' || boardPositions[z+9]=='p') {
                        return false;}}}
            if (h > 0 ) {
                if (boardPositions[z-8]=='k') {
                    return false;}
                if (g > 0) {
                    if (boardPositions[z-9]=='k') {
                        return false;}}
                if (g < 7) {
                    if (boardPositions[z-7]=='k') {
                        return false;}}}
            if (g > 0) {
                if (boardPositions[z-1]=='k') {
                    return false;}}
            if (g < 7) {
                if (boardPositions[z+1]=='k') {
                    return false;}}
            // End white king is safe.
            } else {
            for (int i = 0; i < 64; i++) {
                switch (boardPositions[i]) {
                    case 'k': z = i;break;
                }
            }
            int g = z%8;
            int h = z/8;
            bool notI=true;
            // Bishop or Queen
            int k;
            if (h < 7) {
                // Up diagonal moves.
                if (g < 7) {
                    k = z + 9;
                    while (boardPositions[k] == '-' && notI) {
                        if (k/8 < 7 && k%8 < 7) {
                            k = k + 9;
                        } else {notI = false;}} // While it's empty.
                    if (boardPositions[k]=='B'||boardPositions[k]=='Q') {
                        return false;} // When there is an enemy.
                }
                notI = true;
                if (g > 0) {
                    k = z + 7;
                    while (boardPositions[k] == '-' && notI) {
                        if (k%8 > 0 && k/8 < 7) {
                            k = k + 7;
                        } else {notI = false;}} // While it's empty.
                    if (boardPositions[k]=='B'||boardPositions[k]=='Q') {
                        return false;} // When there is an enemy.
                }}

            if (h > 0) {
                // down diagonal moves.
                notI = true;
                if (g > 0) {
                    k = z - 9;
                    while (boardPositions[k] == '-' && notI) {
                        if (k%8 > 0 && k/8 > 0) {
                            k = k - 9;
                        } else {notI = false;}} // While it's empty.
                    if (boardPositions[k]=='B'||boardPositions[k]=='Q') {
                        return false;} // When there is an enemy.
                }
                notI = true;
                if (g < 7) {
                    k = z - 7;
                    while (boardPositions[k] == '-' && notI) {
                        if (k%8 < 7 && k/8 > 0) {
                            k = k - 7;
                        } else { notI = false;}} // While it's empty.
                    if (boardPositions[k]=='B'||boardPositions[k]=='Q') {
                        return false;} // When there is an enemy.
                }}
            // Rook or Queen
            // Up moves
            notI = true;
            int j = 1;
            int vert = 8;
            k = z;
            if (z < 56) {
                k = z + (vert * j);
            }
            while (boardPositions[k] == '-' && notI) {
                vert += 8;
                if (k < 56) {
                    k = z + (vert * j);
                } else {notI = false;}} // While it's empty.
            if (boardPositions[k]=='R'||boardPositions[k]=='Q') {
                return false;} // When there is an enemy..

            // Down moves
            notI = true;
            j = -1;
            vert = 8;
            k = z;
            if (z > 7) {
                k = z + (vert * j);
            }
            while (boardPositions[k] == '-' && notI) {
                vert += 8;
                if (k >7) {
                    k = z + (vert * j);
                } else {notI = false;}} // While it's empty.
            if (boardPositions[k]=='R'||boardPositions[k]=='Q') {
                return false;} // When there is an enemy..

            // Right side....
            notI = true;
            int rj = 1;
            int rk = z;
            if (g < 7) {
                rk = z + rj;
            }
            while (boardPositions[rk] == '-' && notI) {
                rj++;
                if (rk%8 < 7) {
                    rk = z + rj;
                } else {notI = false;}} // While it's empty.
            if (boardPositions[rk]=='R'||boardPositions[rk]=='Q') {
                return false;} // When there is an enemy..

            // Left side....
            notI=true;
            rj = 1;
            rk = z;
            if (g > 0) {
                rk = z - rj;
            }
            while (boardPositions[rk] == '-' && notI) {
                rj++;
                if (rk%8 > 0) {
                    rk = z - rj;
                } else {notI = false;}} // While it's empty.
            if (boardPositions[rk]=='R'||boardPositions[rk]=='Q') {
                return false;} // When there is an enemy..
            // Knight
            if (h < 7 ) {
                if (g > 1 && boardPositions[z+6]=='N') {
                    return false;}
                if (g < 6 && boardPositions[z+10]=='N') {
                    return false;}}
            if (h < 6 ) {
                if (g > 0 && boardPositions[z+15]=='N') {
                    return false;}
                if (g < 7 && boardPositions[z+17]=='N') {
                    return false;}}
            if (h > 0 ) {
                if (g < 6 && boardPositions[z-6]=='N') {
                    return false;}
                if (g > 1 && boardPositions[z-10]=='N') {
                    return false;}}
            if (h > 1 ) {
                if (g < 7 && boardPositions[z-15]=='N') {
                    return false;}
                if (g > 0 && boardPositions[z-17]=='N') {
                    return false;}}
            // King check // Don't move next to another king!
            if (h < 7 ) {
                if (boardPositions[z+8]=='K') {
                    return false;}
                if (g > 0) {
                    if (boardPositions[z+7]=='K') {
                        return false;}}
                if (g < 7) {
                    if (boardPositions[z+9]=='K') {
                        return false;}}}
            if (h > 0 ) {
                if (boardPositions[z-8]=='K') {
                    return false;}
                if (g > 0) {
                    if (boardPositions[z-9]=='K' || boardPositions[z-9]=='P') {
                        return false;}}
                if (g < 7) {
                    if (boardPositions[z-7]=='K' || boardPositions[z-7]=='P') {
                        return false;}}}
            if (g > 0) {
                if (boardPositions[z-1]=='K') {
                    return false;}}
            if (g < 7) {
                if (boardPositions[z+1]=='K') {
                    return false;}}
            // End black king is safe.
        }
        // Nothing returned false, so we know the king is safe.
        return true;
    } // End is king safe?

///////////////////////////////////////////////////////////////////////

string Moves::nightMovesB(string boardPositions, int i) {
        string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = false;

        int rowNum = i/8;
        int colNum = i%8;

        if (rowNum < 7 ) {
            if (colNum > 1) {
                theseMoves.push_back(i + 6);
            }
            if (colNum < 6) {
                theseMoves.push_back(i+10);
            }
        }
        if (rowNum < 6 ) {
            if (colNum > 0) {
                theseMoves.push_back(i + 15);
            }
            if (colNum < 7) {
                theseMoves.push_back(i+17);
            }
        }
        if (rowNum > 0 ) {
            if (colNum < 6) {
                theseMoves.push_back(i - 6);
            }
            if (colNum > 1) {
                theseMoves.push_back(i-10);
            }
        }
        if (rowNum > 1 ) {
            if (colNum < 7) {
                theseMoves.push_back(i - 15);
            }
            if (colNum > 0) {
                theseMoves.push_back(i-17);
            }
        }

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (isupper(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'n';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'n';
            }
        }
        return list;
    } // End black knight moves.
    
string Moves::rookMovesB(string boardPositions, int i) {
		string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = false;
		int rowNum = i/8;
        int colNum = i%8;
        int g = i%8;
        // Up moves
        bool notI = true;
        int j = 1;
        int vert = 8;
        int k = i;
        if (i < 56) {
            k = i + (vert * j);
        }
        while (theBoard[k] == '-' && notI) {
            theseMoves.push_back(k);
            vert += 8;
            if (k < 56) {
                k = i + (vert * j);
            } else {
                notI = false;
            }
        } // While it's empty.
        if (isupper(theBoard[k])) {
            theseMoves.push_back(k);
        } // When there is an enemy.

        // Down moves
        notI = true;
        j = -1;
        vert = 8;
        k = i;
        if (i > 7) {
            k = i + (vert * j);
        }
        while (theBoard[k] == '-' && notI) {
            theseMoves.push_back(k);
            vert += 8;
            if (k >7) {
                k = i + (vert * j);
            } else {
                notI = false;
            }
        } // While it's empty.
        if (isupper(theBoard[k])) {
            theseMoves.push_back(k);
        } // When there is an enemy.

        // Right side....
        notI = true;
        int rj = 1;
        int rk = i;
        if (g < 7) {
            rk = i + rj;
        }
        while (theBoard[rk] == '-' && notI) {
            theseMoves.push_back(rk);
            rj++;
            if (rk%8 < 7) {
                rk = i + rj;
            } else {
                notI = false;
            }
        } // While it's empty.
        if (isupper(theBoard[rk])) {
            theseMoves.push_back(rk);
        } // When there is an enemy.

        // Left side....
        notI=true;
        rj = 1;
        rk = i;
        if (g > 0) {
            rk = i - rj;
        }
        while (theBoard[rk] == '-' && notI) {
            theseMoves.push_back(rk);
            rj++;
            if (rk%8 > 0) {
                rk = i - rj;
            } else {
                notI=false;
            }
        } // While it's empty.
        if (isupper(theBoard[rk])) {
            theseMoves.push_back(rk);
        } // When there is an enemy.

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (isupper(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'r';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'r';
            }
        }
        return list;
    } // End black rook moves.
    
string Moves::bishopMovesB(string boardPositions, int i) {
        bool notI=true;
        int rowNum = i/8;
        int colNum = i%8;
        string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = false;

        int k;
        if (rowNum < 7) {
            // Up diagonal moves.
            if (colNum < 7) {
                k = i + 9;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k/8 < 7 && k%8 < 7) {
                        k = k + 9;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (isupper(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
            notI = true;
            if (colNum > 0) {
                k = i + 7;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 > 0 && k/8 < 7) {
                        k = k + 7;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (isupper(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
        }

        if (rowNum > 0) {
            // down diagonal moves.
            notI = true;
            if (colNum > 0) {
                k = i - 9;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 > 0 && k/8 > 0) {
                        k = k - 9;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (isupper(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
            notI = true;
            if (colNum < 7) {
                k = i - 7;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 < 7 && k/8 > 0) {
                        k = k - 7;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (isupper(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
        }

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (isupper(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'b';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'b';
            }
        }
        return list;
    } // End Bishop moves.
    
    string Moves::queenMovesB(string boardPositions, int i) {
		string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = false;
		int rowNum = i/8;
        int colNum = i%8;
        int g = i%8;
        // Up moves
        bool notI = true;
        int j = 1;
        int vert = 8;
        int k = i;
        if (i < 56) {
            k = i + (vert * j);
        }
        while (theBoard[k] == '-' && notI) {
            theseMoves.push_back(k);
            vert += 8;
            if (k < 56) {
                k = i + (vert * j);
            } else {
                notI = false;
            }
        } // While it's empty.
        if (isupper(theBoard[k])) {
            theseMoves.push_back(k);
        } // When there is an enemy.

        // Down moves
        notI = true;
        j = -1;
        vert = 8;
        k = i;
        if (i > 7) {
            k = i + (vert * j);
        }
        while (theBoard[k] == '-' && notI) {
            theseMoves.push_back(k);
            vert += 8;
            if (k >7) {
                k = i + (vert * j);
            } else {
                notI = false;
            }
        } // While it's empty.
        if (isupper(theBoard[k])) {
            theseMoves.push_back(k);
        } // When there is an enemy.

        // Right side....
        notI = true;
        int rj = 1;
        int rk = i;
        if (g < 7) {
            rk = i + rj;
        }
        while (theBoard[rk] == '-' && notI) {
            theseMoves.push_back(rk);
            rj++;
            if (rk%8 < 7) {
                rk = i + rj;
            } else {
                notI = false;
            }
        } // While it's empty.
        if (isupper(theBoard[rk])) {
            theseMoves.push_back(rk);
        } // When there is an enemy.

        // Left side....
        notI=true;
        rj = 1;
        rk = i;
        if (g > 0) {
            rk = i - rj;
        }
        while (theBoard[rk] == '-' && notI) {
            theseMoves.push_back(rk);
            rj++;
            if (rk%8 > 0) {
                rk = i - rj;
            } else {
                notI=false;
            }
        } // While it's empty.
        if (isupper(theBoard[rk])) {
            theseMoves.push_back(rk);
        } // When there is an enemy.
        
        if (rowNum < 7) {
            // Up diagonal moves.
            if (colNum < 7) {
                k = i + 9;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k/8 < 7 && k%8 < 7) {
                        k = k + 9;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (isupper(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
            notI = true;
            if (colNum > 0) {
                k = i + 7;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 > 0 && k/8 < 7) {
                        k = k + 7;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (isupper(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
        }

        if (rowNum > 0) {
            // down diagonal moves.
            notI = true;
            if (colNum > 0) {
                k = i - 9;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 > 0 && k/8 > 0) {
                        k = k - 9;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (isupper(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
            notI = true;
            if (colNum < 7) {
                k = i - 7;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 < 7 && k/8 > 0) {
                        k = k - 7;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (isupper(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
        }

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (isupper(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'q';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'q';
            }
        }
        return list;
    } // End black queen moves.
    
    string Moves::kingMovesB(string boardPositions, int i) {
		string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = false;
		int rowNum = i/8;
        int colNum = i%8;
        if (rowNum > 0) {
            if (theBoard[i-8] == '-' || isupper(theBoard[i-8])) {
                theseMoves.push_back(i-8);}
            if (colNum > 0) {
                if (theBoard[i-9] == '-' || isupper(theBoard[i-9])) {
                    theseMoves.push_back(i-9);}}
            if (colNum < 7) {
                if (theBoard[i-7] == '-' || isupper(theBoard[i-7])) {
                    theseMoves.push_back(i-7);}}}
        if (rowNum < 7) {
            if (theBoard[i+8] == '-' || isupper(theBoard[i+8])) {
                theseMoves.push_back(i+8);}
            if (colNum < 7) {
                if (theBoard[i+9] == '-' || isupper(theBoard[i+9])) {
                    theseMoves.push_back(i+9);}}
            if (colNum > 0) {
                if (theBoard[i+7] == '-' || isupper(theBoard[i+7])) {
                    theseMoves.push_back(i+7);}}}
        if (colNum < 7) {
            if (theBoard[i+1] == '-' || isupper(theBoard[i+1])) {
                theseMoves.push_back(i+1);}}
        if (colNum > 0) {
            if (theBoard[i-1] == '-' || isupper(theBoard[i-1])) {
                theseMoves.push_back(i-1);}}

        // Need castle moves //
        if (theBoard[60] == 'k') {
			//cout << "The king is on square 60" << endl;
			if (theBoard[59] == '-' && theBoard[58] == '-' &&
			theBoard[57] == '-' && theBoard[56] == 'r' && m_qcastle == true) {
				//cout << "Path is clear to castle queenside." << endl;
					if (isKingSafe(theBoard, turn)) {
						//The king is not in check, so...
						theBoard[59] = 'k';
						theBoard[60] = '-';
						if (isKingSafe(theBoard, turn)) {
							//The king is safe in between, so...
							theseMoves.push_back(58);
							//Then the move will get checked if king is safe at destination.
						}
							theBoard[60] = 'k';
							theBoard[59] = '-';}
				} //queenside
			if (theBoard[61] == '-' && theBoard[62] == '-' &&
			theBoard[63] == 'r' && m_Kcastle == true) {
				//cout << "Path is clear to castle kingside." << endl;
					if (isKingSafe(theBoard, turn)) {
						//The king is not in check, so...
						theBoard[61] = 'k';
						theBoard[60] = '-';
						if (isKingSafe(theBoard, turn)) {
							//The king is safe in between, so...
							theseMoves.push_back(62);
							//Then the move will get checked if king is safe at destination.
						}
							theBoard[60] = 'k';
							theBoard[61] = '-';}
				} //kingside	
			} // end castle

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            //cout << theseMoves[l] << endl;
            if (isupper(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'k';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'k';
            }
        }
        return list;
    } // End black king moves.
    
    string Moves::pawnMovesB(string boardPositions, int i, bool isPass, char enPass) {
		string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = false;
		int rowNum = i/8;
        int colNum = i%8;
        int k = i - 16;
        
        if (rowNum == 6) {
			// The standard catch for moving two spaces forward.
            if (theBoard[i-8] == '-' && theBoard[i-16] == '-') {
                theseMoves.push_back(k);
            }
		}
        
            // The standard catch for moving one space forward.
            k = i - 8;
            if (theBoard[k] == '-') {
                theseMoves.push_back(k);
            }
            k = i - 9;// Attacking to the left and down.
            if (colNum > 0 && isupper(theBoard[k])) {
                theseMoves.push_back(k);
            }
            k = i - 7;// Attacking to the right and down.
            if (colNum < 7 && isupper(theBoard[k])) {
                theseMoves.push_back(k);
            }
        // End boring pawn moves.
        // Need en passant moves //
			if (isPass){
			if (i > 23 && i < 32) {
				int passedPawn = 8;
				if (enPass == 'a'){ passedPawn = 0; }
				else if (enPass == 'b'){ passedPawn = 1; }
				else if (enPass == 'c'){ passedPawn = 2; }
				else if (enPass == 'd'){ passedPawn = 3; }
				else if (enPass == 'e'){ passedPawn = 4; }
				else if (enPass == 'f'){ passedPawn = 5; }
				else if (enPass == 'g'){ passedPawn = 6; }
				else if (enPass == 'h'){ passedPawn = 7; }
				if (passedPawn - colNum == 1) {
					theseMoves.push_back(i-7);
					}
				if (colNum - passedPawn == 1) {
					theseMoves.push_back(i-9);
					}
				}}// End en passant moves

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (isupper(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'p';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'p';
            }
        }
        return list;
    } // End black pawn moves.

//---------------------------------------------------------------------

string Moves::nightMoves(string boardPositions, int i) {
        string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = true;

        int rowNum = i/8;
        int colNum = i%8;

        if (rowNum < 7 ) {
            if (colNum > 1) {
                theseMoves.push_back(i + 6);
            }
            if (colNum < 6) {
                theseMoves.push_back(i+10);
            }
        }
        if (rowNum < 6 ) {
            if (colNum > 0) {
                theseMoves.push_back(i + 15);
            }
            if (colNum < 7) {
                theseMoves.push_back(i+17);
            }
        }
        if (rowNum > 0 ) {
            if (colNum < 6) {
                theseMoves.push_back(i - 6);
            }
            if (colNum > 1) {
                theseMoves.push_back(i-10);
            }
        }
        if (rowNum > 1 ) {
            if (colNum < 7) {
                theseMoves.push_back(i - 15);
            }
            if (colNum > 0) {
                theseMoves.push_back(i-17);
            }
        }

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (islower(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'N';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'N';
            }
        }
        return list;
    } // End white knight moves.
    
string Moves::rookMoves(string boardPositions, int i) {
		string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = true;
		int rowNum = i/8;
        int colNum = i%8;
        int g = i%8;
        // Up moves
        bool notI = true;
        int j = 1;
        int vert = 8;
        int k = i;
        if (i < 56) {
            k = i + (vert * j);
        }
        while (theBoard[k] == '-' && notI) {
            theseMoves.push_back(k);
            vert += 8;
            if (k < 56) {
                k = i + (vert * j);
            } else {
                notI = false;
            }
        } // While it's empty.
        if (islower(theBoard[k])) {
            theseMoves.push_back(k);
        } // When there is an enemy.

        // Down moves
        notI = true;
        j = -1;
        vert = 8;
        k = i;
        if (i > 7) {
            k = i + (vert * j);
        }
        while (theBoard[k] == '-' && notI) {
            theseMoves.push_back(k);
            vert += 8;
            if (k >7) {
                k = i + (vert * j);
            } else {
                notI = false;
            }
        } // While it's empty.
        if (islower(theBoard[k])) {
            theseMoves.push_back(k);
        } // When there is an enemy.

        // Right side....
        notI = true;
        int rj = 1;
        int rk = i;
        if (g < 7) {
            rk = i + rj;
        }
        while (theBoard[rk] == '-' && notI) {
            theseMoves.push_back(rk);
            rj++;
            if (rk%8 < 7) {
                rk = i + rj;
            } else {
                notI = false;
            }
        } // While it's empty.
        if (islower(theBoard[rk])) {
            theseMoves.push_back(rk);
        } // When there is an enemy.

        // Left side....
        notI=true;
        rj = 1;
        rk = i;
        if (g > 0) {
            rk = i - rj;
        }
        while (theBoard[rk] == '-' && notI) {
            theseMoves.push_back(rk);
            rj++;
            if (rk%8 > 0) {
                rk = i - rj;
            } else {
                notI=false;
            }
        } // While it's empty.
        if (islower(theBoard[rk])) {
            theseMoves.push_back(rk);
        } // When there is an enemy.

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (islower(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'R';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'R';
            }
        }
        return list;
    } // End white rook moves.
    
    
string Moves::bishopMoves(string boardPositions, int i) {
        bool notI=true;
        int rowNum = i/8;
        int colNum = i%8;
        string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = true;

        int k;
        if (rowNum < 7) {
            // Up diagonal moves.
            if (colNum < 7) {
                k = i + 9;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k/8 < 7 && k%8 < 7) {
                        k = k + 9;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (islower(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
            notI = true;
            if (colNum > 0) {
                k = i + 7;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 > 0 && k/8 < 7) {
                        k = k + 7;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (islower(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
        }

        if (rowNum > 0) {
            // down diagonal moves.
            notI = true;
            if (colNum > 0) {
                k = i - 9;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 > 0 && k/8 > 0) {
                        k = k - 9;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (islower(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
            notI = true;
            if (colNum < 7) {
                k = i - 7;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 < 7 && k/8 > 0) {
                        k = k - 7;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (islower(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
        }

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (islower(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'B';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'B';
            }
        }
        return list;
    } // End Bishop moves.
    
    string Moves::queenMoves(string boardPositions, int i) {
		string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = true;
		int rowNum = i/8;
        int colNum = i%8;
        int g = i%8;
        // Up moves
        bool notI = true;
        int j = 1;
        int vert = 8;
        int k = i;
        if (i < 56) {
            k = i + (vert * j);
        }
        while (theBoard[k] == '-' && notI) {
            theseMoves.push_back(k);
            vert += 8;
            if (k < 56) {
                k = i + (vert * j);
            } else {
                notI = false;
            }
        } // While it's empty.
        if (islower(theBoard[k])) {
            theseMoves.push_back(k);
        } // When there is an enemy.

        // Down moves
        notI = true;
        j = -1;
        vert = 8;
        k = i;
        if (i > 7) {
            k = i + (vert * j);
        }
        while (theBoard[k] == '-' && notI) {
            theseMoves.push_back(k);
            vert += 8;
            if (k >7) {
                k = i + (vert * j);
            } else {
                notI = false;
            }
        } // While it's empty.
        if (islower(theBoard[k])) {
            theseMoves.push_back(k);
        } // When there is an enemy.

        // Right side....
        notI = true;
        int rj = 1;
        int rk = i;
        if (g < 7) {
            rk = i + rj;
        }
        while (theBoard[rk] == '-' && notI) {
            theseMoves.push_back(rk);
            rj++;
            if (rk%8 < 7) {
                rk = i + rj;
            } else {
                notI = false;
            }
        } // While it's empty.
        if (islower(theBoard[rk])) {
            theseMoves.push_back(rk);
        } // When there is an enemy.

        // Left side....
        notI=true;
        rj = 1;
        rk = i;
        if (g > 0) {
            rk = i - rj;
        }
        while (theBoard[rk] == '-' && notI) {
            theseMoves.push_back(rk);
            rj++;
            if (rk%8 > 0) {
                rk = i - rj;
            } else {
                notI=false;
            }
        } // While it's empty.
        if (islower(theBoard[rk])) {
            theseMoves.push_back(rk);
        } // When there is an enemy.
        
        if (rowNum < 7) {
            // Up diagonal moves.
            if (colNum < 7) {
                k = i + 9;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k/8 < 7 && k%8 < 7) {
                        k = k + 9;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (islower(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
            notI = true;
            if (colNum > 0) {
                k = i + 7;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 > 0 && k/8 < 7) {
                        k = k + 7;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (islower(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
        }

        if (rowNum > 0) {
            // down diagonal moves.
            notI = true;
            if (colNum > 0) {
                k = i - 9;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 > 0 && k/8 > 0) {
                        k = k - 9;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (islower(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
            notI = true;
            if (colNum < 7) {
                k = i - 7;
                while (theBoard[k] == '-' && notI) {
                    theseMoves.push_back(k);
                    if (k%8 < 7 && k/8 > 0) {
                        k = k - 7;
                    } else {
                        notI = false;
                    }
                } // While it's empty.
                if (islower(theBoard[k])) {
                    theseMoves.push_back(k);
                } // When there is an enemy.
            }
        }

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (islower(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'Q';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'Q';
            }
        }
        return list;
    } // End white queen moves.
    
    string Moves::kingMoves(string boardPositions, int i) {
		string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = true;
		int rowNum = i/8;
        int colNum = i%8;
        if (rowNum > 0) {
            if (theBoard[i-8] == '-' || islower(theBoard[i-8])) {
                theseMoves.push_back(i-8);}
            if (colNum > 0) {
                if (theBoard[i-9] == '-' || islower(theBoard[i-9])) {
                    theseMoves.push_back(i-9);}}
            if (colNum < 7) {
                if (theBoard[i-7] == '-' || islower(theBoard[i-7])) {
                    theseMoves.push_back(i-7);}}}
        if (rowNum < 7) {
            if (theBoard[i+8] == '-' || islower(theBoard[i+8])) {
                theseMoves.push_back(i+8);}
            if (colNum < 7) {
                if (theBoard[i+9] == '-' || islower(theBoard[i+9])) {
                    theseMoves.push_back(i+9);}}
            if (colNum > 0) {
                if (theBoard[i+7] == '-' || islower(theBoard[i+7])) {
                    theseMoves.push_back(i+7);}}}
        if (colNum < 7) {
            if (theBoard[i+1] == '-' || islower(theBoard[i+1])) {
                theseMoves.push_back(i+1);}}
        if (colNum > 0) {
            if (theBoard[i-1] == '-' || islower(theBoard[i-1])) {
                theseMoves.push_back(i-1);}}

        // Need castle moves //
        if (theBoard[4] == 'K') {
			//cout << "The king is on square 4" << endl;
			if (theBoard[3] == '-' && theBoard[2] == '-' &&
			theBoard[1] == '-' && theBoard[0] == 'R' && m_Qcastle == true) {
				//cout << "Path is clear to castle queenside." << endl;
					if (isKingSafe(theBoard, turn)) {
						//The king is not in check, so...
						theBoard[3] = 'K';
						theBoard[4] = '-';
						if (isKingSafe(theBoard, turn)) {
							//The king is safe in between, so...
							theseMoves.push_back(2);
							//Then the move will get checked if king is safe at destination.
						}
							theBoard[4] = 'K';
							theBoard[3] = '-';}
				} //queenside
			if (theBoard[5] == '-' && theBoard[6] == '-' &&
			theBoard[7] == 'R' && m_Kcastle == true) {
				//cout << "Path is clear to castle kingside." << endl;
					if (isKingSafe(theBoard, turn)) {
						//The king is not in check, so...
						theBoard[5] = 'K';
						theBoard[4] = '-';
						if (isKingSafe(theBoard, turn)) {
							//The king is safe in between, so...
							theseMoves.push_back(6);
							//Then the move will get checked if king is safe at destination.
						}
							theBoard[4] = 'K';
							theBoard[5] = '-';}
				} //kingside	
			} // end castle

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (islower(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'K';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'K';
            }
        }
        return list;
    } // End white king moves.
    
    string Moves::pawnMoves(string boardPositions, int i, bool isPass, char enPass) {
		string list = "";
        vector <int> theseMoves;
        string moveSquare;
        string theBoard = boardPositions;
        bool turn = true;
		int rowNum = i/8;
        int colNum = i%8;
        int k = i + 16;
        if (rowNum == 1) {
			// The standard catch for moving two spaces forward.
            if (theBoard[i+8] == '-' && theBoard[i+16] == '-') {
                theseMoves.push_back(k);
            }
		}

            // The standard catch for moving one space forward.
            k = i + 8;
            if (theBoard[k] == '-') {
                theseMoves.push_back(k);
            }
            k = i + 7;// Attacking to the left and up.
            if (colNum > 0 && islower(theBoard[k])) {
                theseMoves.push_back(k);
            }
            k = i + 9;// Attacking to the right and up.
            if (colNum < 7 && islower(theBoard[k])) {
                theseMoves.push_back(k);
            }
        // End boring pawn moves.
        // Need en passant moves //
        //if (m_enPassant){cout << "en passant flagged" << endl;
			if (isPass){
			if (i > 31 && i < 40) {
				int passedPawn = 8;
				if (enPass == 'a'){ passedPawn = 0; }
				else if (enPass == 'b'){ passedPawn = 1; }
				else if (enPass == 'c'){ passedPawn = 2; }
				else if (enPass == 'd'){ passedPawn = 3; }
				else if (enPass == 'e'){ passedPawn = 4; }
				else if (enPass == 'f'){ passedPawn = 5; }
				else if (enPass == 'g'){ passedPawn = 6; }
				else if (enPass == 'h'){ passedPawn = 7; }
				if (passedPawn - colNum == 1) {
					theseMoves.push_back(i+9);
					}
				if (colNum - passedPawn == 1) {
					theseMoves.push_back(i+7);
					}
				}}// End en passant moves

        for(int l=0; (unsigned)l<theseMoves.size();l++) {
            int k = theseMoves[l];
            if (islower(theBoard[k]) || theBoard[k] == '-') {
                moveSquare = theBoard[k];
                theBoard[k] = 'P';
                theBoard[i] = '-';
                if (isKingSafe(theBoard, turn)) {
					char F = (char)('a' + colNum);
					char G = (char)('1' + rowNum);
					int rowNumK = k/8;
					int colNumK = k%8;
					char T = (char)('a' + colNumK);
					char U = (char)('1' + rowNumK);
                    list = list +  F + G + T + U + ",";
                }
                theBoard[k] = moveSquare[0];
                theBoard[i] = 'P';
            }
        }
        return list;
    } // End white pawn moves.
    
//====================================================================

string Moves::available(string boardPositions, bool whoseTurn, bool isPass, char enPass) {
	string list = "";
        if (whoseTurn){
            for (int i = 0; i < 64; i++) {
                switch (boardPositions[i]) {
                    case 'N': list+=nightMoves(boardPositions, i);break;
                    case 'R': list+=rookMoves(boardPositions, i);break;
                    case 'B': list+=bishopMoves(boardPositions, i);break;
                    case 'Q': list+=queenMoves(boardPositions, i);break;
                    case 'K': list+=kingMoves(boardPositions, i);break;
                    case 'P': list+=pawnMoves(boardPositions, i, isPass, enPass);break;
                }
            }} else {
            for (int i = 0; i < 64; i++) {
                switch (boardPositions[i]) {
                    case 'n': list+=nightMovesB(boardPositions, i);break;
                    case 'r': list+=rookMovesB(boardPositions, i);break;
                    case 'b': list+=bishopMovesB(boardPositions, i);break;
                    case 'q': list+=queenMovesB(boardPositions, i);break;
                    case 'k': list+=kingMovesB(boardPositions, i);break;
                    case 'p': list+=pawnMovesB(boardPositions, i, isPass, enPass);break;
                }
            }}
        //Debugging only // 
        //cout << list << endl;
        return list;
        /*
         * The list is in this format 1234,
         * 1,2 = 2 digit from square
         * 3,4 = 2 digit to square
         * followed by a comma.
         */
}

string Moves::nonPlyMove(string boardPositions, bool whoseTurn, bool isPass, char enPass){
	string candidateList = Moves::available(boardPositions, whoseTurn, isPass, enPass);
	string chosenMove = "";
	int currentEval = 0;
		if (candidateList.size() > 0){
			currentEval = evaluations.getEval(boardPositions);
			int moveEval = 0;
			int bestEval = currentEval;
			for (int a = 0; a < 4; a++) {
				chosenMove += candidateList[(0)+a];
				}
			for (int e = 0; unsigned(e) < (candidateList.size()/5); e++){
				string proposedMove = "";
				for (int a = 0; a < 4; a++) {
				proposedMove += candidateList[(5*e)+a];
				}
					int first =((proposedMove.at(0) - 'a' + 1) + (((proposedMove.at(1) - '1') * 8) - 1));
					int second =((proposedMove.at(2) - 'a' + 1) + (((proposedMove.at(3) - '1') * 8) - 1));
					char firstPieceHolder = boardPositions[first];
					char secondPieceHolder = boardPositions[second];
					boardPositions[second] = boardPositions[first];
					boardPositions[first] = '-';
					moveEval = evaluations.getEval(boardPositions);
					if (whoseTurn){
						// White's turn, higher is better.
						if ( moveEval > bestEval ) {
							chosenMove = proposedMove;
							bestEval = moveEval;
							} else if ( moveEval == bestEval && (int)time(0)%2 == 0) {
								chosenMove = proposedMove;
								bestEval = moveEval;
								}
						} else {
							// Black's turn, lower is better.
							if ( moveEval < bestEval ) {
							chosenMove = proposedMove;
							bestEval = moveEval;
							} else if ( moveEval == bestEval && (int)time(0)%2 == 0) {
								chosenMove = proposedMove;
								bestEval = moveEval;
								}
							}
							boardPositions[first] = firstPieceHolder;
							boardPositions[second] = secondPieceHolder;
				} // End candidate list moves.
		} else {
		// candidate list is 0.... game over.
		}
		//cout << " second move " << chosenMove << endl;
	return chosenMove;
}

string makeMoves (string proposedMove, string boardPositions){
	string boardReturned = boardPositions;
	try{
		int first =((proposedMove.at(0) - 'a' + 1) + (((proposedMove.at(1) - '1') * 8) - 1));
		int second =((proposedMove.at(2) - 'a' + 1) + (((proposedMove.at(3) - '1') * 8) - 1));
		boardPositions[second] = boardPositions[first];
		boardPositions[first] = '-';
		boardReturned = boardPositions;
	} catch (...) {
	cout << " Exception " << endl;
	} // End try/catch block
	return boardReturned;
}
	


string Moves::plyMove(string boardPositions, bool whoseTurn, bool isPass, char enPass, int plyMoves){
	string originalBoard = boardPositions;
	string playBoard = boardPositions;
	string tempBoard = boardPositions;
	string nextMove = "";
	string candidateList = Moves::available(boardPositions, whoseTurn, isPass, enPass);
	string chosenMove = "";
	int currentEval = evaluations.getEval(boardPositions);
		if (candidateList.size() > 0){
			int bestEval = 0;
			if (whoseTurn){bestEval-=8000;} else {bestEval+=8000;}
			for (int a = 0; a < 4; a++) {
				chosenMove += candidateList[(0)+a];
				}
			for (int e = 0; unsigned(e) < (candidateList.size()/5); e++){
				string proposedMove = "";
				for (int a = 0; a < 4; a++) {
				proposedMove += candidateList[(5*e)+a];
				}
				playBoard = makeMoves(proposedMove, originalBoard);
				currentEval = evaluations.getEval(playBoard);
				cout << " current eval and ply " << currentEval << " " << plyMoves << endl;
				if (whoseTurn) {
					if (currentEval > 9000) {cout << " ply  ends 10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} else {
						if (currentEval < -9000) {cout << " ply  ends -10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} 
				if (plyMoves-1 < 1){ 
						cout << " ply  ends 1." << endl;
					if (whoseTurn){
						// White's turn, higher is better.
						if ( currentEval > bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
						}
						} else {
						// Black's turn, lower is better.
						if ( currentEval < bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							}
						}
					} else {
				// Start 2 ply.
				nextMove = nonPlyMove(playBoard, !whoseTurn, isPass, enPass);
				if (nextMove.size() < 4){chosenMove = proposedMove;
					return chosenMove;}
				playBoard = makeMoves(nextMove, playBoard);
				currentEval = evaluations.getEval(playBoard);
				cout << " current eval and ply " << currentEval << " " << plyMoves << endl;
				if (whoseTurn) {
					if (currentEval > 9000) {cout << " ply  ends 10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} else {
						if (currentEval < -9000) {cout << " ply  ends -10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} 
				if (plyMoves-2 < 1){
					if (whoseTurn){
						// White's turn, higher is better.
						if ( currentEval > bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
						}
						} else {
						// Black's turn, lower is better.
						if ( currentEval < bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							}
						}
					} else {
				// Start 3 ply.
				nextMove = nonPlyMove(playBoard, whoseTurn, isPass, enPass);
				playBoard = makeMoves(nextMove, playBoard);
				currentEval = evaluations.getEval(playBoard);
				cout << " current eval and ply " << currentEval << " " << plyMoves << endl;
				if (whoseTurn) {
					if (currentEval > 9000) {cout << " ply  ends 10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} else {
						if (currentEval < -9000) {cout << " ply  ends -10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} 
				if (plyMoves-3 < 1){
					if (whoseTurn){
						// White's turn, higher is better.
						if ( currentEval > bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
						}
						} else {
						// Black's turn, lower is better.
						if ( currentEval < bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							}
						}
					} else {
				// Start 4 ply.
				nextMove = nonPlyMove(playBoard, !whoseTurn, isPass, enPass);
				if (nextMove.size() < 4){chosenMove = proposedMove;
					return chosenMove;}
				playBoard = makeMoves(nextMove, playBoard);
				currentEval = evaluations.getEval(playBoard);
				cout << " current eval and ply " << currentEval << " " << plyMoves << endl;
				if (whoseTurn) {
					if (currentEval > 9000) {cout << " ply  ends 10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} else {
						if (currentEval < -9000) {cout << " ply  ends -10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} 
				if (plyMoves-4 < 1){
					if (whoseTurn){
						// White's turn, higher is better.
						if ( currentEval > bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
						}
						} else {
						// Black's turn, lower is better.
						if ( currentEval < bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							}
						}
					} else {
				// Start 5 ply.
				nextMove = nonPlyMove(playBoard, whoseTurn, isPass, enPass);
				playBoard = makeMoves(nextMove, playBoard);
				currentEval = evaluations.getEval(playBoard);
				cout << " current eval and ply " << currentEval << " " << plyMoves << endl;
				if (whoseTurn) {
					if (currentEval > 9000) {cout << " ply  ends 10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} else {
						if (currentEval < -9000) {cout << " ply  ends -10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} 
				if (plyMoves-5 < 1){
					if (whoseTurn){
						// White's turn, higher is better.
						if ( currentEval > bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
						}
						} else {
						// Black's turn, lower is better.
						if ( currentEval < bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							}
						}
					} else {
				// Start 6 ply.
				nextMove = nonPlyMove(playBoard, !whoseTurn, isPass, enPass);
				if (nextMove.size() < 4){chosenMove = proposedMove;
					return chosenMove;}
				playBoard = makeMoves(nextMove, playBoard);
				currentEval = evaluations.getEval(playBoard);
				cout << " current eval and ply " << currentEval << " " << plyMoves << endl;
				if (whoseTurn) {
					if (currentEval > 9000) {cout << " ply  ends 10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} else {
						if (currentEval < -9000) {cout << " ply  ends -10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} 
				if (plyMoves-6 < 1){
					if (whoseTurn){
						// White's turn, higher is better.
						if ( currentEval > bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
						}
						} else {
						// Black's turn, lower is better.
						if ( currentEval < bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							}
						}
					} else {
				// Start 7 ply.
				nextMove = nonPlyMove(playBoard, whoseTurn, isPass, enPass);
				playBoard = makeMoves(nextMove, playBoard);
				currentEval = evaluations.getEval(playBoard);
				cout << " current eval and ply " << currentEval << " " << plyMoves << endl;
				if (whoseTurn) {
					if (currentEval > 9000) {cout << " ply  ends 10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} else {
						if (currentEval < -9000) {cout << " ply  ends -10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} 
				if (plyMoves-7 < 1){
					if (whoseTurn){
						// White's turn, higher is better.
						if ( currentEval > bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
						}
						} else {
						// Black's turn, lower is better.
						if ( currentEval < bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							}
						}
					} else {
				// Start 8 ply.
				nextMove = nonPlyMove(playBoard, !whoseTurn, isPass, enPass);
				if (nextMove.size() < 4){chosenMove = proposedMove;
					return chosenMove;}
				playBoard = makeMoves(nextMove, playBoard);
				currentEval = evaluations.getEval(playBoard);
				cout << " current eval and ply " << currentEval << " " << plyMoves << endl;
				if (whoseTurn) {
					if (currentEval > 9000) {cout << " ply  ends 10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} else {
						if (currentEval < -9000) {cout << " ply  ends -10000." << endl;
					chosenMove = proposedMove;
					return chosenMove;
					}} 
				if (plyMoves-8 < 1){
					if (whoseTurn){
						// White's turn, higher is better.
						if ( currentEval > bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
						}
						} else {
						// Black's turn, lower is better.
						if ( currentEval < bestEval ) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							} else if ( currentEval == bestEval && (int)time(0)%2 == 0) {
							chosenMove = proposedMove; 
							bestEval = currentEval;
							cout <<"info depth " << plyMoves << " score cp "<< currentEval << " pv " << chosenMove  << endl; //<< " comment " << boardPositions 
							}
						}
					} else {
						
					} // End 8 ply.
					} // End 7 ply.
					} // End 6 ply.
					} // End 5 ply.
					} // End 4 ply.
					} // End 3 ply.
					} // End 2 ply.
					} // End 1 ply.
				
				} // End candidate list moves.
		} else {
		// candidate list is 0.... game over.
		}
	return chosenMove;
}

string Moves::bestMove(string boardPositions, bool whoseTurn, bool styleRandom, bool isPass, char enPass, int plyMoves){
	string candidateList = Moves::available(boardPositions, whoseTurn, isPass, enPass);
	string chosenMove = "";
		if (candidateList.size() > 0){
		if (styleRandom) {
			// Make a random move.
			srand((int)time(0) + candidateList.size());
			int r = (rand() % (candidateList.size()/5));
				for (int a = 0; a < 4; a++) {
				chosenMove += candidateList[(5*r)+a];
				} // End random move.
		} else {
			// Not random, so make educated moves.
			try {
				chosenMove = plyMove(boardPositions, whoseTurn, isPass, enPass, plyMoves);
			} catch (...) {
			cout << " Exception " << endl;
			} // End try/catch block
			}// End not random.
			if (chosenMove == "") {
			// Chosen move is still blank, and list is not 0, that is wrong, so pick a random move.
			chosenMove = bestMove(boardPositions, whoseTurn, true, isPass, enPass, plyMoves);}
		}// End candidates list.
	return chosenMove;
}
